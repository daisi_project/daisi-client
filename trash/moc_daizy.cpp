/****************************************************************************
** Meta object code from reading C++ file 'daizy.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.3.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "daizy.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'daizy.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.3.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_Daizy_t
{
    QByteArrayData data[98];
    char           stringdata[1664];
};
#define QT_MOC_LITERAL(idx, ofs, len)                                                                                  \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(                                                           \
        len, qptrdiff(offsetof(qt_meta_stringdata_Daizy_t, stringdata) + ofs - idx * sizeof(QByteArrayData)))
static const qt_meta_stringdata_Daizy_t qt_meta_stringdata_Daizy = {
    {QT_MOC_LITERAL(0, 0, 5),      QT_MOC_LITERAL(1, 6, 24),     QT_MOC_LITERAL(2, 31, 0),
     QT_MOC_LITERAL(3, 32, 16),    QT_MOC_LITERAL(4, 49, 4),     QT_MOC_LITERAL(5, 54, 10),
     QT_MOC_LITERAL(6, 65, 20),    QT_MOC_LITERAL(7, 86, 32),    QT_MOC_LITERAL(8, 119, 4),
     QT_MOC_LITERAL(9, 124, 22),   QT_MOC_LITERAL(10, 147, 17),  QT_MOC_LITERAL(11, 165, 22),
     QT_MOC_LITERAL(12, 188, 15),  QT_MOC_LITERAL(13, 204, 12),  QT_MOC_LITERAL(14, 217, 10),
     QT_MOC_LITERAL(15, 228, 13),  QT_MOC_LITERAL(16, 242, 9),   QT_MOC_LITERAL(17, 252, 8),
     QT_MOC_LITERAL(18, 261, 15),  QT_MOC_LITERAL(19, 277, 13),  QT_MOC_LITERAL(20, 291, 13),
     QT_MOC_LITERAL(21, 305, 24),  QT_MOC_LITERAL(22, 330, 14),  QT_MOC_LITERAL(23, 345, 27),
     QT_MOC_LITERAL(24, 373, 16),  QT_MOC_LITERAL(25, 390, 18),  QT_MOC_LITERAL(26, 409, 22),
     QT_MOC_LITERAL(27, 432, 12),  QT_MOC_LITERAL(28, 445, 17),  QT_MOC_LITERAL(29, 463, 18),
     QT_MOC_LITERAL(30, 482, 13),  QT_MOC_LITERAL(31, 496, 11),  QT_MOC_LITERAL(32, 508, 12),
     QT_MOC_LITERAL(33, 521, 24),  QT_MOC_LITERAL(34, 546, 29),  QT_MOC_LITERAL(35, 576, 23),
     QT_MOC_LITERAL(36, 600, 27),  QT_MOC_LITERAL(37, 628, 32),  QT_MOC_LITERAL(38, 661, 22),
     QT_MOC_LITERAL(39, 684, 20),  QT_MOC_LITERAL(40, 705, 19),  QT_MOC_LITERAL(41, 725, 15),
     QT_MOC_LITERAL(42, 741, 22),  QT_MOC_LITERAL(43, 764, 18),  QT_MOC_LITERAL(44, 783, 13),
     QT_MOC_LITERAL(45, 797, 13),  QT_MOC_LITERAL(46, 811, 10),  QT_MOC_LITERAL(47, 822, 24),
     QT_MOC_LITERAL(48, 847, 26),  QT_MOC_LITERAL(49, 874, 6),   QT_MOC_LITERAL(50, 881, 17),
     QT_MOC_LITERAL(51, 899, 19),  QT_MOC_LITERAL(52, 919, 23),  QT_MOC_LITERAL(53, 943, 15),
     QT_MOC_LITERAL(54, 959, 17),  QT_MOC_LITERAL(55, 977, 13),  QT_MOC_LITERAL(56, 991, 13),
     QT_MOC_LITERAL(57, 1005, 8),  QT_MOC_LITERAL(58, 1014, 11), QT_MOC_LITERAL(59, 1026, 20),
     QT_MOC_LITERAL(60, 1047, 21), QT_MOC_LITERAL(61, 1069, 16), QT_MOC_LITERAL(62, 1086, 16),
     QT_MOC_LITERAL(63, 1103, 15), QT_MOC_LITERAL(64, 1119, 19), QT_MOC_LITERAL(65, 1139, 5),
     QT_MOC_LITERAL(66, 1145, 6),  QT_MOC_LITERAL(67, 1152, 8),  QT_MOC_LITERAL(68, 1161, 16),
     QT_MOC_LITERAL(69, 1178, 18), QT_MOC_LITERAL(70, 1197, 18), QT_MOC_LITERAL(71, 1216, 14),
     QT_MOC_LITERAL(72, 1231, 14), QT_MOC_LITERAL(73, 1246, 18), QT_MOC_LITERAL(74, 1265, 17),
     QT_MOC_LITERAL(75, 1283, 14), QT_MOC_LITERAL(76, 1298, 14), QT_MOC_LITERAL(77, 1313, 9),
     QT_MOC_LITERAL(78, 1323, 13), QT_MOC_LITERAL(79, 1337, 12), QT_MOC_LITERAL(80, 1350, 7),
     QT_MOC_LITERAL(81, 1358, 18), QT_MOC_LITERAL(82, 1377, 26), QT_MOC_LITERAL(83, 1404, 20),
     QT_MOC_LITERAL(84, 1425, 20), QT_MOC_LITERAL(85, 1446, 22), QT_MOC_LITERAL(86, 1469, 13),
     QT_MOC_LITERAL(87, 1483, 8),  QT_MOC_LITERAL(88, 1492, 13), QT_MOC_LITERAL(89, 1506, 16),
     QT_MOC_LITERAL(90, 1523, 10), QT_MOC_LITERAL(91, 1534, 21), QT_MOC_LITERAL(92, 1556, 19),
     QT_MOC_LITERAL(93, 1576, 24), QT_MOC_LITERAL(94, 1601, 7),  QT_MOC_LITERAL(95, 1609, 17),
     QT_MOC_LITERAL(96, 1627, 19), QT_MOC_LITERAL(97, 1647, 16)},
    "Daizy\0listShowResultClickAccel\0\0"
    "QTreeWidgetItem*\0item\0AccelSolve\0"
    "SetOutputSolverFiles\0"
    "clickedBrouseInputSolverFileSlot\0text\0"
    "SetAccelMainParameters\0clickedBrouseSlot\0"
    "SetAccelFlowParameters\0DevelopersEvent\0"
    "LicenseEvent\0AboutEvent\0RFQMatcherOpt\0"
    "RFQAccOpt\0RFQEmOpt\0CalculateRFQAcc\0"
    "ExportToLidos\0RFQinitialOpt\0"
    "SetLinacDynSimParameters\0SimulateDynRFQ\0"
    "listShowRFQCavityParameters\0"
    "QListWidgetItem*\0GenerateRFQForFlow\0"
    "SetLinacFlowParameters\0AddFlowAccel\0"
    "listLinacControls\0SetLinacParameters\0"
    "SetSyncPhases\0SetAccelEff\0AddEmittance\0"
    "ApplyFieldSolverSettings\0"
    "listShowResultClickEmittances\0"
    "AddSetOfPotentialsEvent\0"
    "listShowConductorStateClick\0"
    "ApplySolverEmissionModelSettings\0"
    "ApplyDefaultBoundaries\0SetConditionFromFile\0"
    "ExportDataFileEvent\0ExportDataEvent\0"
    "ExportDataFileEventEps\0ExportDataEventEps\0"
    "ErrorEstimate\0SetMChNumbers\0DeleteFlow\0"
    "ApplyConductorBoundaries\0"
    "ShowConductorSelectionMenu\0number\0"
    "AddConductorEvent\0ApplySolverSettings\0"
    "ApplySolverSettingsComp\0InitFieldSolver\0"
    "SaveDataFileEvent\0SaveDataEvent\0"
    "LoadDataEvent\0Simulate\0updateGrViz\0"
    "ShowCreateNewProject\0CreateNewProjectEvent\0"
    "SaveProjectEvent\0LoadProjectEvent\0"
    "currItemClicked\0onCustomContextMenu\0"
    "point\0Rclick\0QAction*\0AddBoundaryEvent\0"
    "AddBoundariesEvent\0fileBoundaryBrouse\0"
    "LoadModelEvent\0SaveModelEvent\0"
    "SaveModelFileEvent\0AddPotentialEvent\0"
    "rigthList2Left\0leftList2rigth\0listClick\0"
    "applyBoundary\0GenerateMesh\0AddFlow\0"
    "SetDirectionPoints\0SetEmissionParametersLinac\0"
    "SetEmitterParameters\0ApplyEmitterBoundary\0"
    "listShowFlowStateClick\0Vizualization\0"
    "updateGr\0FieldSimulate\0GenerateGeometry\0"
    "SaveParams\0AddFlowConditionEvent\0"
    "listShowResultClick\0listShowResultClickLinac\0"
    "AddPlot\0listShowPlotClick\0listShowPlot2dClick\0"
    "AbortSimulations"};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Daizy[] = {

    // content:
    7,      // revision
    0,      // classname
    0, 0,   // classinfo
    89, 14, // methods
    0, 0,   // properties
    0, 0,   // enums/sets
    0, 0,   // constructors
    0,      // flags
    0,      // signalCount

    // slots: name, argc, parameters, tag, flags
    1, 2, 459, 2, 0x0a /* Public */, 5, 0, 464, 2, 0x0a /* Public */, 6, 0, 465, 2, 0x0a /* Public */, 7, 1, 466, 2,
    0x0a /* Public */, 9, 0, 469, 2, 0x0a /* Public */, 10, 1, 470, 2, 0x0a /* Public */, 11, 0, 473, 2,
    0x0a /* Public */, 12, 0, 474, 2, 0x0a /* Public */, 13, 0, 475, 2, 0x0a /* Public */, 14, 0, 476, 2,
    0x0a /* Public */, 15, 0, 477, 2, 0x0a /* Public */, 16, 0, 478, 2, 0x0a /* Public */, 17, 0, 479, 2,
    0x0a /* Public */, 18, 0, 480, 2, 0x0a /* Public */, 19, 0, 481, 2, 0x0a /* Public */, 20, 0, 482, 2,
    0x0a /* Public */, 21, 0, 483, 2, 0x0a /* Public */, 22, 0, 484, 2, 0x0a /* Public */, 23, 1, 485, 2,
    0x0a /* Public */, 25, 0, 488, 2, 0x0a /* Public */, 26, 0, 489, 2, 0x0a /* Public */, 27, 0, 490, 2,
    0x0a /* Public */, 28, 1, 491, 2, 0x0a /* Public */, 29, 0, 494, 2, 0x0a /* Public */, 30, 0, 495, 2,
    0x0a /* Public */, 31, 0, 496, 2, 0x0a /* Public */, 32, 0, 497, 2, 0x0a /* Public */, 33, 0, 498, 2,
    0x0a /* Public */, 34, 2, 499, 2, 0x0a /* Public */, 35, 0, 504, 2, 0x0a /* Public */, 36, 1, 505, 2,
    0x0a /* Public */, 37, 0, 508, 2, 0x0a /* Public */, 38, 0, 509, 2, 0x0a /* Public */, 39, 0, 510, 2,
    0x0a /* Public */, 40, 0, 511, 2, 0x0a /* Public */, 41, 0, 512, 2, 0x0a /* Public */, 42, 0, 513, 2,
    0x0a /* Public */, 43, 0, 514, 2, 0x0a /* Public */, 44, 0, 515, 2, 0x0a /* Public */, 45, 0, 516, 2,
    0x0a /* Public */, 46, 0, 517, 2, 0x0a /* Public */, 47, 0, 518, 2, 0x0a /* Public */, 48, 1, 519, 2,
    0x0a /* Public */, 50, 0, 522, 2, 0x0a /* Public */, 51, 0, 523, 2, 0x0a /* Public */, 52, 0, 524, 2,
    0x0a /* Public */, 53, 0, 525, 2, 0x0a /* Public */, 54, 0, 526, 2, 0x0a /* Public */, 55, 0, 527, 2,
    0x0a /* Public */, 56, 0, 528, 2, 0x0a /* Public */, 57, 0, 529, 2, 0x0a /* Public */, 58, 0, 530, 2,
    0x0a /* Public */, 59, 0, 531, 2, 0x0a /* Public */, 60, 0, 532, 2, 0x0a /* Public */, 61, 0, 533, 2,
    0x0a /* Public */, 62, 0, 534, 2, 0x0a /* Public */, 63, 2, 535, 2, 0x0a /* Public */, 64, 1, 540, 2,
    0x0a /* Public */, 66, 1, 543, 2, 0x0a /* Public */, 68, 0, 546, 2, 0x0a /* Public */, 69, 0, 547, 2,
    0x0a /* Public */, 70, 0, 548, 2, 0x0a /* Public */, 71, 0, 549, 2, 0x0a /* Public */, 72, 0, 550, 2,
    0x0a /* Public */, 73, 0, 551, 2, 0x0a /* Public */, 74, 0, 552, 2, 0x0a /* Public */, 75, 0, 553, 2,
    0x0a /* Public */, 76, 0, 554, 2, 0x0a /* Public */, 77, 1, 555, 2, 0x0a /* Public */, 78, 0, 558, 2,
    0x0a /* Public */, 79, 0, 559, 2, 0x0a /* Public */, 80, 0, 560, 2, 0x0a /* Public */, 81, 0, 561, 2,
    0x0a /* Public */, 82, 0, 562, 2, 0x0a /* Public */, 83, 0, 563, 2, 0x0a /* Public */, 84, 0, 564, 2,
    0x0a /* Public */, 85, 1, 565, 2, 0x0a /* Public */, 86, 0, 568, 2, 0x0a /* Public */, 87, 0, 569, 2,
    0x0a /* Public */, 88, 0, 570, 2, 0x0a /* Public */, 89, 0, 571, 2, 0x0a /* Public */, 90, 0, 572, 2,
    0x0a /* Public */, 91, 0, 573, 2, 0x0a /* Public */, 92, 2, 574, 2, 0x0a /* Public */, 93, 2, 579, 2,
    0x0a /* Public */, 94, 0, 584, 2, 0x0a /* Public */, 95, 1, 585, 2, 0x0a /* Public */, 96, 1, 588, 2,
    0x0a /* Public */, 97, 0, 591, 2, 0x0a /* Public */,

    // slots: parameters
    QMetaType::Void, 0x80000000 | 3, QMetaType::Int, 4, 2, QMetaType::Void, QMetaType::Void, QMetaType::Void,
    QMetaType::QString, 8, QMetaType::Void, QMetaType::Void, QMetaType::QString, 8, QMetaType::Void, QMetaType::Void,
    QMetaType::Void, QMetaType::Void, QMetaType::Void, QMetaType::Void, QMetaType::Void, QMetaType::Void,
    QMetaType::Void, QMetaType::Void, QMetaType::Void, QMetaType::Void, QMetaType::Void, 0x80000000 | 24, 4,
    QMetaType::Void, QMetaType::Void, QMetaType::Void, QMetaType::Void, 0x80000000 | 24, 4, QMetaType::Void,
    QMetaType::Void, QMetaType::Void, QMetaType::Void, QMetaType::Void, QMetaType::Void, 0x80000000 | 3, QMetaType::Int,
    4, 2, QMetaType::Void, QMetaType::Void, 0x80000000 | 24, 4, QMetaType::Void, QMetaType::Void, QMetaType::Void,
    QMetaType::Void, QMetaType::Void, QMetaType::Void, QMetaType::Void, QMetaType::Void, QMetaType::Void,
    QMetaType::Void, QMetaType::Void, QMetaType::Void, QMetaType::Int, 49, QMetaType::Void, QMetaType::Void,
    QMetaType::Void, QMetaType::Void, QMetaType::Void, QMetaType::Void, QMetaType::Void, QMetaType::Void,
    QMetaType::Void, QMetaType::Void, QMetaType::Void, QMetaType::Void, QMetaType::Void, QMetaType::Void,
    0x80000000 | 3, QMetaType::Int, 2, 2, QMetaType::Void, QMetaType::QPoint, 65, QMetaType::Void, 0x80000000 | 67, 2,
    QMetaType::Void, QMetaType::Void, QMetaType::Void, QMetaType::Void, QMetaType::Void, QMetaType::Void,
    QMetaType::Void, QMetaType::Void, QMetaType::Void, QMetaType::Void, 0x80000000 | 24, 2, QMetaType::Void,
    QMetaType::Void, QMetaType::Void, QMetaType::Void, QMetaType::Void, QMetaType::Void, QMetaType::Void,
    QMetaType::Void, 0x80000000 | 24, 2, QMetaType::Void, QMetaType::Void, QMetaType::Void, QMetaType::Void,
    QMetaType::Void, QMetaType::Void, QMetaType::Void, 0x80000000 | 3, QMetaType::Int, 4, 2, QMetaType::Void,
    0x80000000 | 3, QMetaType::Int, 4, 2, QMetaType::Void, QMetaType::Void, 0x80000000 | 24, 4, QMetaType::Void,
    0x80000000 | 24, 4, QMetaType::Void,

    0 // eod
};

void Daizy::qt_static_metacall(QObject* _o, QMetaObject::Call _c, int _id, void** _a)
{
    if (_c == QMetaObject::InvokeMetaMethod)
    {
        Daizy* _t = static_cast<Daizy*>(_o);
        switch (_id)
        {
        case 0:
            _t->listShowResultClickAccel((*reinterpret_cast<QTreeWidgetItem*(*)>(_a[1])),
                                         (*reinterpret_cast<int(*)>(_a[2])));
            break;
        case 1:
            _t->AccelSolve();
            break;
        case 2:
            _t->SetOutputSolverFiles();
            break;
        case 3:
            _t->clickedBrouseInputSolverFileSlot((*reinterpret_cast<const QString(*)>(_a[1])));
            break;
        case 4:
            _t->SetAccelMainParameters();
            break;
        case 5:
            _t->clickedBrouseSlot((*reinterpret_cast<const QString(*)>(_a[1])));
            break;
        case 6:
            _t->SetAccelFlowParameters();
            break;
        case 7:
            _t->DevelopersEvent();
            break;
        case 8:
            _t->LicenseEvent();
            break;
        case 9:
            _t->AboutEvent();
            break;
        case 10:
            _t->RFQMatcherOpt();
            break;
        case 11:
            _t->RFQAccOpt();
            break;
        case 12:
            _t->RFQEmOpt();
            break;
        case 13:
            _t->CalculateRFQAcc();
            break;
        case 14:
            _t->ExportToLidos();
            break;
        case 15:
            _t->RFQinitialOpt();
            break;
        case 16:
            _t->SetLinacDynSimParameters();
            break;
        case 17:
            _t->SimulateDynRFQ();
            break;
        case 18:
            _t->listShowRFQCavityParameters((*reinterpret_cast<QListWidgetItem*(*)>(_a[1])));
            break;
        case 19:
            _t->GenerateRFQForFlow();
            break;
        case 20:
            _t->SetLinacFlowParameters();
            break;
        case 21:
            _t->AddFlowAccel();
            break;
        case 22:
            _t->listLinacControls((*reinterpret_cast<QListWidgetItem*(*)>(_a[1])));
            break;
        case 23:
            _t->SetLinacParameters();
            break;
        case 24:
            _t->SetSyncPhases();
            break;
        case 25:
            _t->SetAccelEff();
            break;
        case 26:
            _t->AddEmittance();
            break;
        case 27:
            _t->ApplyFieldSolverSettings();
            break;
        case 28:
            _t->listShowResultClickEmittances((*reinterpret_cast<QTreeWidgetItem*(*)>(_a[1])),
                                              (*reinterpret_cast<int(*)>(_a[2])));
            break;
        case 29:
            _t->AddSetOfPotentialsEvent();
            break;
        case 30:
            _t->listShowConductorStateClick((*reinterpret_cast<QListWidgetItem*(*)>(_a[1])));
            break;
        case 31:
            _t->ApplySolverEmissionModelSettings();
            break;
        case 32:
            _t->ApplyDefaultBoundaries();
            break;
        case 33:
            _t->SetConditionFromFile();
            break;
        case 34:
            _t->ExportDataFileEvent();
            break;
        case 35:
            _t->ExportDataEvent();
            break;
        case 36:
            _t->ExportDataFileEventEps();
            break;
        case 37:
            _t->ExportDataEventEps();
            break;
        case 38:
            _t->ErrorEstimate();
            break;
        case 39:
            _t->SetMChNumbers();
            break;
        case 40:
            _t->DeleteFlow();
            break;
        case 41:
            _t->ApplyConductorBoundaries();
            break;
        case 42:
            _t->ShowConductorSelectionMenu((*reinterpret_cast<int(*)>(_a[1])));
            break;
        case 43:
            _t->AddConductorEvent();
            break;
        case 44:
            _t->ApplySolverSettings();
            break;
        case 45:
            _t->ApplySolverSettingsComp();
            break;
        case 46:
            _t->InitFieldSolver();
            break;
        case 47:
            _t->SaveDataFileEvent();
            break;
        case 48:
            _t->SaveDataEvent();
            break;
        case 49:
            _t->LoadDataEvent();
            break;
        case 50:
            _t->Simulate();
            break;
        case 51:
            _t->updateGrViz();
            break;
        case 52:
            _t->ShowCreateNewProject();
            break;
        case 53:
            _t->CreateNewProjectEvent();
            break;
        case 54:
            _t->SaveProjectEvent();
            break;
        case 55:
            _t->LoadProjectEvent();
            break;
        case 56:
            _t->currItemClicked((*reinterpret_cast<QTreeWidgetItem*(*)>(_a[1])), (*reinterpret_cast<int(*)>(_a[2])));
            break;
        case 57:
            _t->onCustomContextMenu((*reinterpret_cast<const QPoint(*)>(_a[1])));
            break;
        case 58:
            _t->Rclick((*reinterpret_cast<QAction*(*)>(_a[1])));
            break;
        case 59:
            _t->AddBoundaryEvent();
            break;
        case 60:
            _t->AddBoundariesEvent();
            break;
        case 61:
            _t->fileBoundaryBrouse();
            break;
        case 62:
            _t->LoadModelEvent();
            break;
        case 63:
            _t->SaveModelEvent();
            break;
        case 64:
            _t->SaveModelFileEvent();
            break;
        case 65:
            _t->AddPotentialEvent();
            break;
        case 66:
            _t->rigthList2Left();
            break;
        case 67:
            _t->leftList2rigth();
            break;
        case 68:
            _t->listClick((*reinterpret_cast<QListWidgetItem*(*)>(_a[1])));
            break;
        case 69:
            _t->applyBoundary();
            break;
        case 70:
            _t->GenerateMesh();
            break;
        case 71:
            _t->AddFlow();
            break;
        case 72:
            _t->SetDirectionPoints();
            break;
        case 73:
            _t->SetEmissionParametersLinac();
            break;
        case 74:
            _t->SetEmitterParameters();
            break;
        case 75:
            _t->ApplyEmitterBoundary();
            break;
        case 76:
            _t->listShowFlowStateClick((*reinterpret_cast<QListWidgetItem*(*)>(_a[1])));
            break;
        case 77:
            _t->Vizualization();
            break;
        case 78:
            _t->updateGr();
            break;
        case 79:
            _t->FieldSimulate();
            break;
        case 80:
            _t->GenerateGeometry();
            break;
        case 81:
            _t->SaveParams();
            break;
        case 82:
            _t->AddFlowConditionEvent();
            break;
        case 83:
            _t->listShowResultClick((*reinterpret_cast<QTreeWidgetItem*(*)>(_a[1])),
                                    (*reinterpret_cast<int(*)>(_a[2])));
            break;
        case 84:
            _t->listShowResultClickLinac((*reinterpret_cast<QTreeWidgetItem*(*)>(_a[1])),
                                         (*reinterpret_cast<int(*)>(_a[2])));
            break;
        case 85:
            _t->AddPlot();
            break;
        case 86:
            _t->listShowPlotClick((*reinterpret_cast<QListWidgetItem*(*)>(_a[1])));
            break;
        case 87:
            _t->listShowPlot2dClick((*reinterpret_cast<QListWidgetItem*(*)>(_a[1])));
            break;
        case 88:
            _t->AbortSimulations();
            break;
        default:;
        }
    }
}

const QMetaObject Daizy::staticMetaObject = {
    {&QMainWindow::staticMetaObject, qt_meta_stringdata_Daizy.data, qt_meta_data_Daizy, qt_static_metacall, 0, 0}};

const QMetaObject* Daizy::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void* Daizy::qt_metacast(const char* _clname)
{
    if (!_clname)
        return 0;
    if (!strcmp(_clname, qt_meta_stringdata_Daizy.stringdata))
        return static_cast<void*>(const_cast<Daizy*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int Daizy::qt_metacall(QMetaObject::Call _c, int _id, void** _a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod)
    {
        if (_id < 89)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 89;
    }
    else if (_c == QMetaObject::RegisterMethodArgumentMetaType)
    {
        if (_id < 89)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 89;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
